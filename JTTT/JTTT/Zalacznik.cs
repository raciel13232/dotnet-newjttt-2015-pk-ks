﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JTTT
{
    public class Zalacznik
    {
        public int Id { get; set; }
        String _Description;
        Lista<TextImg> _Zalaczniki;

        public Zalacznik()
        {
            _Zalaczniki = new Lista<TextImg>();
        }

        public String Description { set { _Description = value; } get { return _Description; } }

        public Lista<TextImg> Zalaczniki { set { _Zalaczniki = value; } get { return _Zalaczniki; } }

        public string ZalacznikiToHtml()
        {
            String tmp = "";
            foreach (var i in _Zalaczniki)
            {
                tmp += "<br/> <img src=" + i.Img.Sciezka + ">";
            }
            return tmp;
        }
    }
}

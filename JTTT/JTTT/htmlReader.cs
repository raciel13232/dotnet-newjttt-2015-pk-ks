﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Windows;
using System.Net;

namespace JTTT
{
    public class htmlReader
    {
        string _inhtml;
        HtmlDocument _html;


        public htmlReader()
        {
            _html = new HtmlDocument();
        }

        public bool GetHtml(String strona)
        {
            try
            {
                using (WebClient wc = new WebClient())
                {
                    byte[] data = wc.DownloadData(strona);
                    _inhtml = System.Net.WebUtility.HtmlDecode(Encoding.UTF8.GetString(data));
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void Get(Zalacznik attach, String phrase)
        {
            _html.LoadHtml(_inhtml);
            var nodes = _html.DocumentNode.Descendants("img");

                foreach (var node in nodes)
                {
                    String[] hasla = node.GetAttributeValue("alt", "").Split(new Char[] { ' ', '\t', '\n', '-', '.', ',' });
                    foreach (String s in hasla)
                    {
                        if (s == phrase)
                        {
                            try
                            {
                                attach.Zalaczniki.AddNew(new TextImg(node.GetAttributeValue("src", ""), node.GetAttributeValue("alt", "")));
                            }
                            catch
                            {

                            }
                            break;
                        }
                    }
                }
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    [Serializable()]
    public class AdresyEmail: Where
    {
        public Lista<Email> _ListaAdresow;

        public override int Id
        {
            get
            {
                return base.Id;
            }
            set
            {
                base.Id = value;
            }
        }

        public virtual Lista<Email> ListaAdresow
        {
            get { return _ListaAdresow; }
            set { _ListaAdresow = value; }
        }

        public AdresyEmail()
            : base()
        {
            _ListaAdresow = new Lista<Email>();
        }

        public override string ToString()
        {
            return "Email:"+_ListaAdresow.Count.ToString();
        }
    }
}

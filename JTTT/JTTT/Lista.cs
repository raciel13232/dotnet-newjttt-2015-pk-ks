﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;

namespace JTTT
{
    [Serializable]
        public class Lista<T> : List<T>, INotifyCollectionChanged
        {

            public event NotifyCollectionChangedEventHandler CollectionChanged;

            protected void OnChange(NotifyCollectionChangedAction Name)
            {
                if (CollectionChanged != null) CollectionChanged(this, new NotifyCollectionChangedEventArgs(Name));
            }


            public Lista() : base() { }

            public void AddNew(T s)
            {
                base.Add(s);
                OnChange(NotifyCollectionChangedAction.Reset);
            }

            public void RemoveIndex(int index)
            {
                base.Remove(base[index]);
                OnChange(NotifyCollectionChangedAction.Reset);
            }

            public void ClearAll()
            {
                base.Clear();
                OnChange(NotifyCollectionChangedAction.Reset);
            }
        }
}

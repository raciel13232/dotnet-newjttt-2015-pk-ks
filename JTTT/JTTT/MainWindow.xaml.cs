﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace JTTT
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        ViewModel vModel;
        String file;

        public MainWindow()
        {
            file = "Pliki";
            if(!Directory.Exists(file))
            {
                Directory.CreateDirectory(file);
            }
            InitializeComponent();
            vModel = new ViewModel();
            DataContext = vModel;
            try
            {
                using (var db = new JTTTdbContext())
                {
                    foreach (var f in db.Zadanie)
                    {
                        Wiadomosc wiad = new Wiadomosc();
                        wiad.Zad.CoZrobic = f.CoZrobic;
                        wiad.Zad.GdzieWyslac = f.GdzieWyslac;
                        wiad.Zad.CoZrobic.Get(wiad.Zalacznik);
                        if (wiad.Zad.GdzieWyslac is AdresyEmail)
                        {
                            int i = ((AdresyEmail)wiad.Zad.GdzieWyslac).ListaAdresow.Count;
                        }
                        vModel.ListaWiadomosci.AddNew(wiad);
                    }
                }
            }
            catch
            {
                MessageBox.Show("Nie udalo sie wczytac bazy");
            }
        }

        private void Ustaw_Click(object sender, RoutedEventArgs e)
        {
            vModel.UstawImgFinder();
        }

        private void DodajMiastoButton_Click(object sender, RoutedEventArgs e)
        {
            vModel.UstawPogoda();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            vModel.DodajEmail();
        }

        private void UstawEmail_Click(object sender, RoutedEventArgs e)
        {
            vModel.UstawEmail();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            vModel.DodajZadanie();
        }

        private void WczytajButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog OFD = new OpenFileDialog();
            OFD.Filter = "Binary (.bin)|*.bin";
            OFD.InitialDirectory = "Pliki\\";
            bool? result=OFD.ShowDialog();
            
            if (result == true)
            {
                vModel.Wczytaj(OFD.OpenFile());
            }
        }

        private void ZapiszButton_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog SFD = new SaveFileDialog();
            SFD.FileName = "JTTTWorks";
            SFD.DefaultExt = ".bin";
            SFD.Filter = "Binary (.bin)|*.bin";
            Nullable<bool> result=SFD.ShowDialog();

            if (result == true)
            {
                vModel.Zapisz(SFD.FileName);
            }

        }

        private void ListaZadaniaWyczyscButton_Click(object sender, RoutedEventArgs e)
        {
            vModel.ListaWiadomosci.ClearAll();
        }

        private void ListaAdresowWyczyscButton_Click(object sender, RoutedEventArgs e)
        {
            vModel.AdresyEmaili.ClearAll();
        }

        private void ListaZadanUsunButton_Click(object sender, RoutedEventArgs e)
        {
            if (vModel.ListaWiadomosci.Count > 0 && ZadaniaListBox.SelectedItem != null)
            {
                vModel.ListaWiadomosci.RemoveIndex(ZadaniaListBox.SelectedIndex);
            }
        }

        private void ListaAdresowUsunButton_Click(object sender, RoutedEventArgs e)
        {
            if (vModel.AdresyEmaili.Count > 0 && ListaAdresowListBox.SelectedItem != null)
            {
                vModel.AdresyEmaili.RemoveIndex(ListaAdresowListBox.SelectedIndex);
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            vModel.Wyslij();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            vModel.UstawToAppi();
        }

        private void WszystkieButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (var db = new JTTTdbContext())
                {
                    vModel.DBList.ClearAll();
                    foreach (var f in db.Zadanie)
                    {
                        Zadanie Zad = new Zadanie();
                        Zad.CoZrobic = f.CoZrobic;
                        Zad.GdzieWyslac = f.GdzieWyslac;
                        if (Zad.GdzieWyslac is AdresyEmail)
                        {
                            int i = ((AdresyEmail)Zad.GdzieWyslac).ListaAdresow.Count; //by zaladowac dane z LazyLoading
                        }
                        vModel.DBList.AddNew(Zad);
                    }
                }
            }
            catch
            {
                MessageBox.Show("Nie udalo sie wczytac bazy");
            }
        }

        private void DodajFromDB_Click(object sender, RoutedEventArgs e)
        {
            if (WczytywanieDBListBox.SelectedItem != null)
            {
                Zadanie z = new Zadanie();
                z = vModel.DBList[WczytywanieDBListBox.SelectedIndex];
                Wiadomosc wiad = new Wiadomosc();
                wiad.Zad = z;
                wiad.Zad.CoZrobic.Get(wiad.Zalacznik);
                vModel.ListaWiadomosci.AddNew(wiad);
            }
        }

        private void SzukajButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (var db = new JTTTdbContext())
                {
                    vModel.DBList.ClearAll();
                    foreach (var f in db.Zadanie)
                    {
                        if (f.CoZrobic is ImgFinder)
                        {
                            if ((vModel.StronaSearch == ((ImgFinder)f.CoZrobic).Html) || vModel.FrazaSearch == ((ImgFinder)f.CoZrobic).Fraza)
                            {
                                Zadanie Zad = new Zadanie();
                                Zad.CoZrobic = f.CoZrobic;
                                Zad.GdzieWyslac = f.GdzieWyslac;
                                if (Zad.GdzieWyslac is AdresyEmail)
                                {
                                    int i = ((AdresyEmail)Zad.GdzieWyslac).ListaAdresow.Count;
                                }
                                vModel.DBList.AddNew(Zad);
                            }
                        }
                        if(f.CoZrobic is Pogoda)
                        {
                            if (vModel.MiastoSearch == ((Pogoda)f.CoZrobic).Miasto)
                            {
                                Zadanie Zad = new Zadanie();
                                Zad.CoZrobic = f.CoZrobic;
                                Zad.GdzieWyslac = f.GdzieWyslac;
                                if (Zad.GdzieWyslac is AdresyEmail)
                                {
                                    int i = ((AdresyEmail)Zad.GdzieWyslac).ListaAdresow.Count;
                                }
                                vModel.DBList.AddNew(Zad);
                            }
                        }
                    }
                }
            }
            catch
            {
                MessageBox.Show("Nie udalo sie wczytac bazy");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    [Serializable()]
    public class ToAppi: Where
    {
      
        String _Appi;

        public override int Id
        {
            get
            {
                return base.Id;
            }
            set
            {
                base.Id = value;
            }
        }

        public ToAppi(): base()
        {
            _Appi = "ToAppi";
        }

        public String Appi
        {
            get { return _Appi; }
            set { _Appi = value; }
        }

        public override string ToString()
        {
            return _Appi;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows;

namespace JTTT
{
    public class TextImg
    {
        Obrazek _Img;
        String _Text;

        public TextImg()
        {
            _Img = new Obrazek();
            _Text = null;
        }

        public TextImg(String source, String txt)
        {
            _Img = new Obrazek(source);
            _Text = txt;
        }

        public Obrazek Img { set { _Img = value; } get { return _Img; } }
        public ImageSource ImgSource { get { return _Img.Obraz; } }
        public String Text { set { _Text = value; } get { return _Text; } }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace JTTT
{
    [Serializable()]
    public class Zadanie
    {
        public int Id { get; set; }
        Work _CoZrobic;
        Where _GdzieWyslac;

        public Zadanie()
        {

        }

        public virtual Work CoZrobic { set { _CoZrobic = value; } get { return _CoZrobic; } }
        public virtual Where GdzieWyslac { set { _GdzieWyslac = value; } get { return _GdzieWyslac; } }

        public override string ToString()
        {
            return _CoZrobic.ToString() + "\t" + _GdzieWyslac.ToString();
        }

    }
}

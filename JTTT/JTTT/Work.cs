﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    [Serializable()]
    public abstract class Work
    {
        public virtual int Id { get; set; }

        public Work() { }

        public override string ToString()
        {
            return "Brak Zadania";
        }

        public virtual bool Get(Zalacznik s) 
        {
            return false;
        }
    }
}

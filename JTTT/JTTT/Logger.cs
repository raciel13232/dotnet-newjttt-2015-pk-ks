﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace JTTT
{
    public class Logger
    {
        String _file;

        public Logger()
        {
            _file = "Logs.log";
        }

        public void NewLog(String log)
        {
            if (!File.Exists(_file))
            {
                File.Create(_file).Close();
            }

            StreamWriter streamlog = new StreamWriter(_file, true);
            streamlog.WriteLine("[" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt") + "]\t" + log + "\n");
            streamlog.Close();
        }
    }
}

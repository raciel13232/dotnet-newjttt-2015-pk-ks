﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JTTT
{
    [Serializable()]
    public class Email
    {
        public int Id { get; set; }
        String _Email;
        public virtual AdresyEmail AdresyEmail { get; set; }

        public Email()
        {
            _Email = null;
        }

        public virtual String EmailAdres
        {
            set { _Email = value; }
            get { return _Email; }
        }

        public override string ToString()
        {
            return _Email;
        }
    }
}

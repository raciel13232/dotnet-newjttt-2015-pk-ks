﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using System.Windows;

namespace JTTT
{
    public class Wiadomosc
    {
        Zadanie _Zad;
        Zalacznik _Zalaczniki;


        public Wiadomosc()
        {
            _Zad = new Zadanie();
            _Zalaczniki = new Zalacznik();
        }

        public Zadanie Zad { set { _Zad = value; } get { return _Zad; } }
        public Zalacznik Zalacznik { set { _Zalaczniki = value; } get { return _Zalaczniki; } }
        public Lista<TextImg> Zalaczniki { get { return _Zalaczniki.Zalaczniki; } }
        public String Fraza
        {
            get
            {
                if(_Zad.CoZrobic is ImgFinder)
                {
                    return ((ImgFinder)_Zad.CoZrobic).Fraza;
                }
                if(_Zad.CoZrobic is Pogoda)
                {
                    return ((Pogoda)_Zad.CoZrobic).Miasto;
                }
                return null;
            }
        }

        public bool Send()
        {
            if(_Zad.GdzieWyslac is AdresyEmail)
            {
                if (_Zalaczniki.Zalaczniki.Count > 0)
                {
                    return SendEmail();
                }
            }
            else if (_Zad.GdzieWyslac is ToAppi)
            {
                if (_Zalaczniki.Zalaczniki.Count > 0)
                {
                    return SendApi();
                }
            }
            return false;
        }

        public bool SendApi()
        {
            NoweOkno okno = new NoweOkno(_Zalaczniki);
            okno.Show();
            return true;
        }

        public bool SendEmail()
        {
            String Me = "JTTT.KS.PK@gmail.com";
            String MyPass = "JTTT12345";

            try
            {
                MailMessage mail = new MailMessage();
                
                mail.From = new MailAddress(Me);
                
                if (((AdresyEmail)_Zad.GdzieWyslac).ListaAdresow.Count > 0)
                {
                    
                    for (int i = 0; i < ((AdresyEmail)_Zad.GdzieWyslac).ListaAdresow.Count; i++)
                    {
                        
                        mail.To.Add(((AdresyEmail)_Zad.GdzieWyslac).ListaAdresow[i].EmailAdres);
                    }
                }
                
                if(_Zad.CoZrobic is ImgFinder)
                {
                    mail.Subject = ((ImgFinder)_Zad.CoZrobic).Fraza;
                }else if (_Zad.CoZrobic is Pogoda)
                {
                    mail.Subject ="Pogoda dla miasta "+((Pogoda)_Zad.CoZrobic).Miasto;
                }

                mail.IsBodyHtml = true;
                mail.Body = _Zalaczniki.Description;
                mail.Body += _Zalaczniki.ZalacznikiToHtml();
                
                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(Me, MyPass);
                
                client.Send(mail);
                return true;
            }
            catch
            {
                //MessageBox.Show("Wiodomosc nie zostala wyslana.");
                return false;
            }
        }
    }
}

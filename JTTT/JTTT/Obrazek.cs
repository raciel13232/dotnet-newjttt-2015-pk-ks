﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows;

namespace JTTT
{
    public class Obrazek
    {
        BitmapImage _Img;
        String _Sciezka;

        public Obrazek()
        {
            _Img = new BitmapImage();
            _Sciezka = null;
        }

        public Obrazek(String source)
        {
            _Img = new BitmapImage();
            _Sciezka = source;
            _Img.BeginInit();
            _Img.UriSource = new Uri(_Sciezka, UriKind.Absolute);
            _Img.EndInit();
        }

        public ImageSource Obraz
        {
            get { return _Img; }
        }

        public String Sciezka
        {
            set 
            {
                _Sciezka = value;
                _Img.BeginInit();
                _Img.UriSource = new Uri(_Sciezka, UriKind.Absolute);
                _Img.EndInit();
            }

            get { return _Sciezka; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace JTTT
{
    [Serializable()]
    public class ImgFinder: Work
    {
        String _Html;
        String _Fraza;

        public override int Id
        {
            get
            {
                return base.Id;
            }
            set
            {
                base.Id = value;
            }
        }

        public ImgFinder()
            : base()
        {
            _Html = "";
            _Fraza = "";
        }

        public String Html
        {
            set { _Html = value; }
            get { return _Html; }
        }

        public String Fraza
        {
            set { _Fraza = value; }
            get { return _Fraza; }
        }

        public override string ToString()
        {
            return _Fraza+"<<"+_Html;
        }

        public override bool Get(Zalacznik s)
        {
            htmlReader html = new htmlReader();
            if(html.GetHtml(_Html))
            {
                html.Get(s, _Fraza);
                if (s.Zalaczniki.Count > 0)
                {
                    s.Description = _Fraza;
                    Logger log = new Logger();
                    log.NewLog("Znaleziono wynik dla: " + ToString());
                    return true;
                }
            }
            Logger log1 = new Logger();
            log1.NewLog("Nie znaleziono wynik dla: " + ToString());
            return false;
        }
    }
}

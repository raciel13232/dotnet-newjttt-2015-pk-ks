﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace JTTT
{
    public class ViewModel:INotifyPropertyChanged
    {
        Lista<Wiadomosc> _ListaWiadomosci;
        ImgFinder _HtmlFraza;
        Pogoda _Pogoda;
        Email _Adres;
        AdresyEmail _AdresyEmail;
        String _FrazaSearch;
        String _StronaSearch;
        String _MiastoSearch;
        Lista<Zadanie> _DBList;
        Zadanie _Zad;

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnChange(string Name)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(Name));
        }

        public ViewModel()
        {
            _ListaWiadomosci = new Lista<Wiadomosc>();
            _HtmlFraza = new ImgFinder();
            _Pogoda = new Pogoda();
            _AdresyEmail = new AdresyEmail();
            _Adres = new Email();
            _Zad=new Zadanie();
            _DBList = new Lista<Zadanie>();
        }

        public Lista<Wiadomosc> ListaWiadomosci     { set { _ListaWiadomosci = value; OnChange("ListaWiadomosci"); }          get { return _ListaWiadomosci; }              } 
        public String Html                          { set { _HtmlFraza.Html = value; OnChange("Html"); }                      get { return _HtmlFraza.Html; }               }
        public String Fraza                         { set { _HtmlFraza.Fraza = value; OnChange("Fraza"); }                    get { return _HtmlFraza.Fraza; }              }
        public String Miasto                        { set { _Pogoda.Miasto = value; OnChange("Miasto"); }                     get { return _Pogoda.Miasto; }                }
        public int Temp                             { set { _Pogoda.Temp = value; OnChange("Temp"); }                         get { return _Pogoda.Temp; }                  }
        public String Adres                         { set { _Adres.EmailAdres = value; OnChange("Adres"); }                   get { return _Adres.EmailAdres; }             }
        public Lista<Email> AdresyEmaili            { set { _AdresyEmail.ListaAdresow = value; OnChange("AdresyEmaili"); }    get { return _AdresyEmail.ListaAdresow; }     }
        public String FrazaSearch                   { set { _FrazaSearch = value; OnChange("FrazaSearch"); }                  get { return _FrazaSearch; }                  }
        public String StronaSearch                  { set { _StronaSearch = value; OnChange("StronaSearch"); }                get { return _StronaSearch; }                 }
        public String MiastoSearch                  { set { _MiastoSearch = value; OnChange("MiastoSearch"); }                get { return _MiastoSearch; }                 }
        public Lista<Zadanie> DBList                { set { _DBList = value; OnChange("DBList"); }                            get { return _DBList; }                       }


        public void UstawImgFinder()
        {
            if(_HtmlFraza.Html!=null && _HtmlFraza.Fraza!=null)
            {
                _Zad.CoZrobic = _HtmlFraza;
                _Pogoda = new Pogoda();
                OnChange("Pogoda");
                OnChange("Temp");
            }
            else
            {
                MessageBox.Show("Nie podano wszystki potrzebnych informacji do ustawienia obiektu");
            }
        }

        public void UstawPogoda()
        {
            if (_Pogoda.Miasto != null && _Pogoda.Temp != 0)
            {
                _Zad.CoZrobic = _Pogoda;
                _HtmlFraza = new ImgFinder();
                OnChange("Fraza");
                OnChange("Html");
            }
            else
            {
                MessageBox.Show("Nie podano wszystki potrzebnych informacji do ustawienia obiektu");
            }
        }

        public void UstawEmail()
        {
            if(_AdresyEmail.ListaAdresow.Count>0)
            {
                _Zad.GdzieWyslac = _AdresyEmail;
            }
            else
            {
                MessageBox.Show("Nie podano zadnych adresow.");
            }
        }

        public void UstawToAppi()
        {
            _Zad.GdzieWyslac = new ToAppi();
            AdresyEmaili.ClearAll();
        }

        public void DodajEmail()
        {
            if(_Adres.EmailAdres!=null)
            {
                _AdresyEmail.ListaAdresow.AddNew(_Adres);
                _Adres = new Email();
                OnChange("Adres");
            }
        }

        public void DodajZadanieZBazy(Zadanie z)
        {
            if ((z.CoZrobic is ImgFinder || z.CoZrobic is Pogoda) && (z.GdzieWyslac is AdresyEmail || z.GdzieWyslac is ToAppi))
            {
                Wiadomosc wiad = new Wiadomosc();
                wiad.Zad = z;
                wiad.Zad.CoZrobic.Get(wiad.Zalacznik);
                _ListaWiadomosci.Add(wiad);
            }
        }

        public void DodajZadanie() 
        {
            if ((_Zad.CoZrobic is ImgFinder || _Zad.CoZrobic is Pogoda) && (_Zad.GdzieWyslac is AdresyEmail || _Zad.GdzieWyslac is ToAppi))
            {
                Wiadomosc wiad = new Wiadomosc();
                using (var db = new JTTTdbContext())
                {
                    db.Zadanie.Add(_Zad);
                    db.SaveChanges();
                }
                wiad.Zad = _Zad;
                wiad.Zad.CoZrobic.Get(wiad.Zalacznik);
                _ListaWiadomosci.AddNew(wiad);

                
                Logger log = new Logger();
                log.NewLog("Nowe zadanie zostalo dodane na liste:"+_Zad.ToString());

                _Zad = new Zadanie();

                _Pogoda = new Pogoda();
                OnChange("Miasto");
                OnChange("Temp");

                _HtmlFraza = new ImgFinder();
                OnChange("Fraza");
                OnChange("Html");

                _AdresyEmail = new AdresyEmail();
                OnChange("AdresyEmaili");
            }
        }

        public void Wyslij()
        {
            foreach (var s in _ListaWiadomosci)
            {
                if (s.Send())
                {
                    Logger log = new Logger();
                    log.NewLog("Wiadomosc o zadaniu: " + s.Zad.ToString() + " zostala wyslana");
                }
            }
            MessageBox.Show("Operacja Zakonczona");
        }

        public void Zapisz(String file)
        {
            if (!File.Exists(file))
            {
                File.Create(file).Close();
            }
            List<Zadanie> listz=new List<Zadanie>();
            foreach (var w in _ListaWiadomosci)
            {
                listz.Add(w.Zad);
            }
            FileStream stream = new FileStream(file, FileMode.OpenOrCreate);
            try 
            {
                BinaryFormatter bin = new BinaryFormatter();
                bin.Serialize(stream, listz);
                MessageBox.Show("Plik zapisany");
            }
            catch 
            {
                MessageBox.Show("Nie udalo sie zapisac pliku");
            }
            finally 
            {
                stream.Close();
            }
        }

        public void Wczytaj(Stream file)
        {
            try
            {
                BinaryFormatter bin = new BinaryFormatter();
                List<Zadanie> listz = new List<Zadanie>();
                listz = (List<Zadanie>)bin.Deserialize(file);
                _ListaWiadomosci.ClearAll();
                foreach (var l in listz)
                {
                    Wiadomosc wiad=new Wiadomosc();
                    wiad.Zad=l;
                    wiad.Zad.CoZrobic.Get(wiad.Zalacznik);
                    _ListaWiadomosci.AddNew(wiad);
                }
                MessageBox.Show("Udalo sie wczytac plik");
            }
            catch
            {
                MessageBox.Show("Nie udalo sie wczytac pliku");
            }
            finally
            {
                file.Close();
            }
            
            
        }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    [Serializable]
    public class Pogoda: Work
    {
        String _Miasto;
        int _Temp;

        public override int Id
        {
            get
            {
                return base.Id;
            }
            set
            {
                base.Id = value;
            }
        }

        public Pogoda()
            : base()
        {
            _Miasto = null;
            _Temp = 0;
        }

        public String Miasto
        {
            set { _Miasto = value; }
            get { return _Miasto; }
        }

        public int Temp
        {
            set { _Temp = value; }
            get { return _Temp; }
        }

        public override string ToString()
        {
            return _Miasto+"<<"+_Temp.ToString();
        }

        double Cel(double Kel)
        {
            return Kel - 273.15;
        }

        public override bool Get(Zalacznik s)
        {
            string json;
            using (WebClient wc = new WebClient())
            {
                json = wc.DownloadString("http://api.openweathermap.org/data/2.5/weather?q="+_Miasto+",pl");
            }
            var weather = JsonConvert.DeserializeObject<WeatherObject>(json);
            if(weather.main.temp>_Temp)
            {
                String page = "http://openweathermap.org/img/w/";
                foreach (var ob in weather.weather)
                {
                    s.Zalaczniki.Add(new TextImg(page+ob.icon+".png", ob.main+"-"+ob.description));
                }
                if (s.Zalaczniki.Count > 0)
                {
                    s.Description = "Temperatura w " + _Miasto + " wynosi " + weather.main.temp + " Kelvinow(" + Cel(weather.main.temp)+" stopni Celcjusza)";

                    Logger log = new Logger();
                    log.NewLog("Znaleziono wynik dla: "+ ToString());
                    return true;
                }
                
            }
            Logger log2 = new Logger();
            log2.NewLog("Nie znaleziono wynik dla: " + ToString());
            return false;
        }
    }
}

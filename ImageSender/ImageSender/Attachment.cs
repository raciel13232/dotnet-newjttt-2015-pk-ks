﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Drawing;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Windows;
using System.Windows.Controls;
using System.Xml;
using System.Xml.Serialization;

namespace ImageSender
{
    [Serializable]
    public class Attachment:INotifyPropertyChanged
    {
        public int Id { get; set; }
        Work _from;
        BitmapImage pic;
        String _imgSorce;
        String _Text;
        public virtual Sender Sender { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnChange(string Name)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(Name));
        }
        
        [Obsolete("Only needed for serialization and materialization", true)]
        public Attachment() { }

        public Attachment(String Sorce, String Txt)
        {
            _from = new Work();
            pic = new BitmapImage();
            pic.BeginInit();
            pic.UriSource = new Uri(Sorce, UriKind.Absolute);
            pic.EndInit();
            _imgSorce = ImgSorce.ToString();
            _Text = Txt;
        }

        [XmlAttribute("FromWork")]
        public Work FromWork
        {
            set { _from = value; }
            get { return _from; }
        }

        [NotMapped]
        public ImageSource ImgSorce
        {
            get { return pic; }
        }

        [XmlAttribute("Pic")]
        [NotMapped]
        public BitmapImage Pic
        {
            set { pic = value; }
            get { return pic; }
        }

        public String ImageSorce
        {
            set
            {
                _imgSorce = value;
            }
            get
            {
                return _imgSorce;
            }
        }

        [XmlAttribute("Text")]
        public String Text
        {
            set { _Text = value; }
            get { return _Text; }
        }

        public override string ToString()
        {
            return _Text;
        }

        public override bool Equals(object obj)
        {
            if (((Attachment)obj).ImgSorce == ImgSorce && ((Attachment)obj).Text == Text)
            {
                return true;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace ImageSender
{
    public class ViewModel : INotifyPropertyChanged
    {
        String _html;
        String _phrase;
        Lista<Work> _works;
        Lista<Attachment> _choose;

        String _name;
        String _newemail;
        String _subject;
        String _body;
        Lista<String> _adresslist;
        Lista<Attachment> _attachments;

        Lista<Sender> _senders;
        Lista<String> _choosenadresslist;
        Lista<Attachment> _choosenattachments;

        Lista<Sender> _dbSenders;
        String _namesearch;
        String _emailsearch;
        String _phrasesearch;

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnChange(string Name)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(Name));
        }

        public ViewModel()
        {
            _html = null;
            _phrase = null;
            _works = new Lista<Work>();
            _choose = new Lista<Attachment>();

            _name = null;
            _newemail = null;
            _subject = null;
            _body = null;
            _adresslist = new Lista<String>();
            _attachments = new Lista<Attachment>();

            _senders = new Lista<Sender>();
            _choosenadresslist = new Lista<String>();
            _choosenattachments = new Lista<Attachment>();

            _dbSenders = new Lista<Sender>();
            _emailsearch = null;
            _namesearch = null;
            _phrasesearch = null;
        }

        public ViewModel(Lista<Sender> lista)
        {
            _html = null;
            _phrase = null;
            _works = new Lista<Work>();
            _choose = new Lista<Attachment>();

            _name = null;
            _newemail = null;
            _subject = null;
            _body = null;
            _adresslist = new Lista<String>();
            _attachments = new Lista<Attachment>();

            _senders = lista;
            _choosenadresslist = new Lista<String>();
            _choosenattachments = new Lista<Attachment>();

            _dbSenders = new Lista<Sender>();
            _emailsearch = null;
            _namesearch = null;
            _phrasesearch = null;
        }

        public String Html
        {
            set { _html = value; OnChange("Html"); }
            get { return _html; }
        }
        public String Phrase
        {
            set { _phrase = value; OnChange("Phrase"); }
            get { return _phrase; }
        }
        public Lista<Work> Works
        {
            set { _works = value; OnChange("Works"); }
            get { return _works; }
        }
        public Lista<Attachment> Choose
        {
            set { _choose = value; OnChange("Choose"); }
            get { return _choose; }
        }

        public String Name
        {
            set { _name = value; OnChange("Name"); }
            get { return _name; }
        }
        public String NewEmail
        {
            set { _newemail = value; OnChange("NewEmail"); }
            get { return _newemail; }
        }
        public String Subject
        {
            set { _subject = value; OnChange("Subject"); }
            get { return _subject; }
        }
        public String Body
        {
            set { _body = value; OnChange("Body"); }
            get { return _body; }
        }
        public Lista<String> AdressList
        {
            set { _adresslist = value; OnChange("AdressList"); }
            get { return _adresslist; }
        }
        public Lista<Attachment> Attachments
        {
            set { _attachments = value; OnChange("Attachments"); }
            get { return _attachments; }
        }

        public Lista<Sender> Senders
        {
            set { _senders = value; OnChange("Senders"); }
            get { return _senders; }
        }
        public Lista<String> ChoosenAdressList
        {
            set { _choosenadresslist = value; OnChange("ChoosenAdressList"); }
            get { return _choosenadresslist; }
        }
        public Lista<Attachment> ChoosenAttachments
        {
            set { _choosenattachments = value; OnChange("ChoosenAttachments"); }
            get { return _choosenattachments; }
        }

        public Lista<Sender> DBView
        {
            set { _dbSenders = value; OnChange("DBView"); }
            get { return _dbSenders; }
        }

        public String NameSearch
        {
            set { _namesearch = value; OnChange("NameSearch"); }
            get { return _namesearch; }
        }

        public String PhraseSearch
        {
            set { _phrasesearch = value; OnChange("PhraseSearch"); }
            get { return _phrasesearch; }
        }

        public String EmailSearch
        {
            set { _emailsearch = value; OnChange("EmailSearch"); }
            get { return _emailsearch; }
        }

        public void New()
        {
            Name = null;
            AdressList = new Lista<String>();
            Attachments = new Lista<Attachment>();
            Subject = null;
            Body = null;
        }

        public Sender ReturnSender()
        {
            Sender s = new Sender("JTTT.KS.PK@gmail.com", "JTTT12345");
            s.Name = Name;
            foreach (string str in AdressList)
            {
                Email e = new Email(str);
                s.Emails.Add(e);
            }
            s.Attachments = Attachments;
            s.Subject = Subject;
            s.Body = Body;
            return s;
        }

        public void GetFromHtml()
        {
            htmlReader reader = new htmlReader();
            foreach (Work w in _works)
            {
                if (reader.GetHtml(w.Html))
                {
                    reader.Get(Choose, w.Phrase);
                }
            }
        }

        public void SendEmail()
        {
            foreach (Sender s in _senders)
            {
                s.Send();
            }
            MessageBox.Show("Wiodomosci wyslane");
        }
    }
}

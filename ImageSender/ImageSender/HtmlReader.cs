﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Windows;
using System.Net;

namespace ImageSender
{
    public class htmlReader
    {
        String _webpage;
        string _inhtml;
        HtmlDocument _html;


        public htmlReader()
        {
            _html = new HtmlDocument();
        }

        public bool GetHtml(String strona)
        {
            
            _webpage = strona;
            try
            {
                using (WebClient wc = new WebClient())
                {
                    byte[] data = wc.DownloadData(_webpage);
                    _inhtml = System.Net.WebUtility.HtmlDecode(Encoding.UTF8.GetString(data));
                }
                return true;
            }
            catch
            {
                MessageBox.Show("Nie udalo sie znalezc strony: "+_webpage);
                return false;
            }
        }

        public void Get(Lista<Attachment> lista, String phrase)
        {
            _html.LoadHtml(_inhtml);
            var nodes = _html.DocumentNode.Descendants("img");

            foreach (var node in nodes)
            {
                String[] hasla = node.GetAttributeValue("alt", "").Split(new Char[] { ' ', '\t', '\n', '-', '.', ',' });
                foreach (String s in hasla)
                {
                    if (s == phrase)
                    {
                        try
                        {
                            Attachment ob = new Attachment(node.GetAttributeValue("src", ""), node.GetAttributeValue("alt", ""));
                            if (ob.ImgSorce != null)
                            {
                                ob.FromWork.Html = _webpage;
                                ob.FromWork.Phrase = phrase;

                                lista.AddNew(ob);

                            }
                        }
                        catch
                        {

                        }
                        break;
                    }
                }
            }
        }
    }
}

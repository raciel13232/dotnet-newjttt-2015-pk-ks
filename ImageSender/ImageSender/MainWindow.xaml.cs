﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ImageSender
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ViewModel vModel;
        Lista<Sender> ListOfSenders;

        public MainWindow()
        {
            ListOfSenders = new Lista<Sender>();
            vModel = new ViewModel(ListOfSenders);
            vModel.New();
            InitializeComponent();
            DataContext = vModel;
        }

        private void AddNewWorkButton_Click(object sender, RoutedEventArgs e)
        {
            if (vModel.Html != null && vModel.Phrase != null)
            {
                Work w = new Work(vModel.Html, vModel.Phrase);
                vModel.Works.AddNew(w);
            }
        }

        private void ClearAllWorksButton_Click(object sender, RoutedEventArgs e)
        {
            vModel.Works.ClearAll();
        }

        private void DeleteWorkButton_Click(object sender, RoutedEventArgs e)
        {
            if (vModel.Works.Count > 0 && WorksListBox.SelectedItem != null)
            {
                vModel.Works.RemoveIndex(WorksListBox.SelectedIndex);
            }
        }

        private void FindWorksButton_Click(object sender, RoutedEventArgs e)
        {
            //Thread
            vModel.GetFromHtml();
        }

        private void ClearAllImgButton_Click(object sender, RoutedEventArgs e)
        {
            vModel.Choose.ClearAll();
        }

        private void AddNewEmailButton_Click(object sender, RoutedEventArgs e)
        {
            if (vModel.NewEmail != null)
            {
                vModel.AdressList.AddNew(vModel.NewEmail);
            }
        }

        private void DeleteEmailButtom_Click(object sender, RoutedEventArgs e)
        {
            if (vModel.AdressList.Count > 0 && AdressListBox.SelectedItem != null)
            {
                vModel.AdressList.RemoveIndex(AdressListBox.SelectedIndex);
            }
        }

        private void ClearAllEmailButton_Click(object sender, RoutedEventArgs e)
        {
            vModel.AdressList.ClearAll();
        }

        private void DeleteAttachmentButton_Click(object sender, RoutedEventArgs e)
        {
            if (vModel.Attachments.Count > 0 && AttachmentsListBox.SelectedItem != null)
            {
                vModel.Attachments.RemoveIndex(AttachmentsListBox.SelectedIndex);
            }
        }

        private void ClearAllAttachmentButton_Click(object sender, RoutedEventArgs e)
        {
            vModel.Attachments.ClearAll();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (vModel.Name != null)
            {
                if (vModel.AdressList.Count > 0)
                {
                    using (var db = new DBContextJttt())
                    {
                        Sender s = new Sender("JTTT.KS.PK@gmail.com", "JTTT12345");
                        s = vModel.ReturnSender();
                        vModel.Senders.AddNew(s);
                        db.DBSender.Add(s);
                        db.SaveChanges();
                        MessageBox.Show("Nowa pozycja zostala dodana:" + vModel.Senders.Last().ToString());
                        vModel.New();
                    }
                }
                else
                {
                    MessageBox.Show("Lista adresow jest pusta");
                }
            }
            else
            {
                MessageBox.Show("Nie podano nazwy wiadomosci");
            }
        }

        private void RemoveSend_Click(object sender, RoutedEventArgs e)
        {
            if (vModel.Senders.Count > 0 && SendsListBox.SelectedItem != null)
            {
                vModel.Senders.RemoveIndex(SendsListBox.SelectedIndex);
            }
        }

        private void ClearAllSends_Click(object sender, RoutedEventArgs e)
        {
            vModel.Senders.ClearAll();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (vModel.Senders.Count > 0)
            {
                vModel.SendEmail();
            }
        }

        private void ChoseImgListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ChoseImgListBox.SelectedItem != null)
            {
                vModel.Attachments.AddNew(vModel.Choose[ChoseImgListBox.SelectedIndex]);
            }
        }

        private void SaveWorksButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void NameSearchButton_Click(object sender, RoutedEventArgs e)
        {
            vModel.DBView.ClearAll();
            if (vModel.NameSearch != null)
            {
                using (var ctx = new DBContextJttt())
                {
                    foreach (var s in ctx.DBSender)
                    {
                        if (s.Name == vModel.NameSearch)
                        {
                            int not = s.Emails.Count;
                            not = s.Attachments.Count;
                            vModel.DBView.AddNew(s);

                        }
                    }
                }

                if (vModel.DBView.Count <= 0)
                {
                    MessageBox.Show("Nie znaleziono zadania: " + vModel.NameSearch);
                }
            }
            else
            {
                MessageBox.Show("Nie podano Nazwy");
            }
        }

        private void EmailSearchButton_Click(object sender, RoutedEventArgs e)
        {
            vModel.DBView.ClearAll();
            if (vModel.EmailSearch != null)
            {
                using (var ctx = new DBContextJttt())
                {
                    foreach (var s in ctx.DBSender)
                    {
                        int not = s.Emails.Count;
                        not = s.Attachments.Count;
                        foreach (Email em in s.Emails)
                        {
                            if (em.Adress == vModel.EmailSearch)
                            {
                                vModel.DBView.AddNew(s);
                            }
                        }
                    }
                }

                if (vModel.DBView.Count <= 0)
                {
                    MessageBox.Show("Zadne z zadan nie zawiera adresu: " + vModel.EmailSearch);
                }
            }
            else
            {
                MessageBox.Show("Nie podano E-mail");
            }
        }

        private void PhraseSearchButton_Click(object sender, RoutedEventArgs e)
        {
            vModel.DBView.ClearAll();
            if (vModel.PhraseSearch != null)
            {
                using (var ctx = new DBContextJttt())
                {
                    foreach (var s in ctx.DBSender)
                    {
                        int not = s.Emails.Count;
                        not = s.Attachments.Count;
                        foreach (Attachment a in s.Attachments)
                        {
                            if (a.FromWork.Phrase == vModel.PhraseSearch)
                            {
                                vModel.DBView.AddNew(s);
                            }
                        }
                    }
                }

                if (vModel.DBView.Count <= 0)
                {
                    MessageBox.Show("Zadne z zadan nie zawiera frazy: " + vModel.PhraseSearch);
                }
            }
            else
            {
                MessageBox.Show("Nie podano Frazy");
            }
        }

        private void DBClearButton_Click(object sender, RoutedEventArgs e)
        {
            vModel.DBView.ClearAll();
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dbSenders.SelectedItem != null)
            {
                vModel.Senders.AddNew(vModel.DBView[dbSenders.SelectedIndex]);
            }
        }
    }
}

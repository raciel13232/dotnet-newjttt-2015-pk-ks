﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageSender
{
    public class Email
    {
        public int Id { get; set; }
        string _adres;
        public virtual Sender Sender { get; set; }

        [Obsolete("Only needed for serialization and materialization", true)]
        public Email() { }

        public Email(string e)
        {
            _adres = e;
        }

        public string Adress
        {
            set { _adres = value; }
            get { return _adres; }
        }
    }
}

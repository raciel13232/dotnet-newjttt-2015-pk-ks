﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;
using System.Xml.Serialization;
using System.ComponentModel.DataAnnotations;

namespace ImageSender
{
    [Serializable]
    public class Sender
    {
        public int Id { get; set; }
        DateTime _Data;
        String _Name;
        String _Me;
        String _MyPass;
        Lista<Email> _Adresses;
        String _Subject;
        String _Body;
        Lista<Attachment> _Attachments;

        [Obsolete("Only needed for serialization and materialization", true)]
        public Sender()
        {
            _Data = new DateTime();
            _Adresses = new Lista<Email>();
            _Attachments = new Lista<Attachment>();
        }

        public Sender(String me, String myPass)
        {
            _Name = null;
            _Data = new DateTime();
            _Data = DateTime.Now;
            _Me = me;
            _MyPass = myPass;
            _Adresses = new Lista<Email>();
            _Subject = "Message from "+_Me;
            _Body = "Something for you:";
            _Attachments = new Lista<Attachment>();
        }

        public DateTime Date
        {
            set { _Data = value; }
            get { return _Data; }
        }

        [XmlAttribute("Name")]
        public String Name
        {
            set { _Name = value; }
            get { return _Name; }
        }

        [XmlAttribute("Me")]
        public String Me
        {
            set { _Me = value; }
            get { return _Me; }            
        }

        [XmlAttribute("MyPass")]
        public String MyPass
        {
            set { _MyPass = value; }
            get { return _MyPass; }
        }

        [XmlAttribute("Adresses")]
        public virtual Lista<Email> Emails
        {
            set { _Adresses = value; }
            get { return _Adresses; }
        }

        [XmlAttribute("Subject")]
        public String Subject
        {
            set { _Subject = value; }
            get { return _Subject; }
        }

        [XmlAttribute("Body")]
        public String Body
        {
            set { _Body = value; }
            get { return _Body; }
        }

        [XmlAttribute("Attachments")]
        public virtual Lista<Attachment> Attachments
        {
            set { _Attachments = value; }
            get { return _Attachments; }
        }

        public bool Send()
        {
            try
            {
                MailMessage mail = new MailMessage();

                mail.From = new MailAddress(_Me);

                if (_Adresses.Count > 0)
                {
                    for (int i = 0; i < _Adresses.Count; i++)
                    {
                        mail.To.Add(_Adresses[i].Adress);
                    }
                }
                else
                {
                    MessageBox.Show("Nie podano zadnego adresu");
                    return false;
                }

                if (_Subject != null)
                {
                    mail.Subject = _Subject;
                }
                else
                {
                    mail.Subject = "You must see this";
                }

                mail.IsBodyHtml = true;

                String TmpBody;
                if (_Body != null)
                {
                    TmpBody = _Body;
                }
                else
                {
                    TmpBody = "Hi ;)";
                }

                for (int i = 0; i < _Attachments.Count; i++)
                {
                    TmpBody += "<br/> <img src=" + _Attachments[i].ImageSorce + ">";
                }

                mail.Body = TmpBody;

                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(_Me, _MyPass);

                client.Send(mail);
                return true;
            }
            catch
            {
                MessageBox.Show("Wiodomosc nie zostala wyslana.");
                return false;
            }
        }

        public override string ToString()
        {  
            return Name+"\tEmails:"+_Adresses.Count.ToString()+"\tAttachments:"+_Attachments.Count.ToString();
        }
    }
}

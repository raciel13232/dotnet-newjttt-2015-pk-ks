﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImageSender
{
    public class Work
    {
        String _html;
        String _Phrase;

        public Work(String html, String phrase)
        {
            _html = html;
            _Phrase = phrase;
        }

        public Work()
        {
            _html = null;
            _Phrase = null;
        }

        public String Html
        {
            set { _html = value; }
            get { return _html; }
        }

        public String Phrase
        {
            set { _Phrase = value; }
            get { return _Phrase; }
        }

        public override string ToString()
        {
            return _Phrase+" <<< "+_html;
        }

        public override bool Equals(object obj)
        {
            if (((Work)obj).Html == Html && ((Work)obj).Phrase == Phrase)
            {
                return true;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}

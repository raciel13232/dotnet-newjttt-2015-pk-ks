﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;

namespace ImageSender
{
    [Serializable]
    public class Lista<T>: List<T>, INotifyCollectionChanged
    {

        public event NotifyCollectionChangedEventHandler CollectionChanged;

        protected void OnChange(NotifyCollectionChangedAction Name)
        {
            if (CollectionChanged != null) CollectionChanged(this, new NotifyCollectionChangedEventArgs(Name));
        }

        
        public Lista(): base(){}

        public void AddNew(T s)
        {
            for (int i = 0; i < base.Count; i++)
            {
                if (s.Equals(base[i]))
                {
                    return;
                }
            }
                base.Add(s);
                OnChange(NotifyCollectionChangedAction.Reset);
        }

        public void RemoveIndex(int index)
        {
            base.Remove(base[index]);
            OnChange(NotifyCollectionChangedAction.Reset);
        }

        public void ClearAll()
        {
            base.Clear();
            OnChange(NotifyCollectionChangedAction.Reset);
        }
    }
}
